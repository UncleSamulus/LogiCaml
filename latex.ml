include Formule ;;

let rec formula2latex formule =
  match formule with
  | Var(e) -> e
  | Neg(e) -> (String.escaped "\neg ") ^ formula2latex e
  | Or(a, b) -> "(" ^ formula2latex a ^ ")" ^ (String.escaped "\vee") ^ "(" ^ (formula2latex b) ^ ")"
  | And(a, b) -> "(" ^ formula2latex a ^ ")" ^ (String.escaped "\wedge") ^ "(" ^ (formula2latex b) ^ ")"
  | Implies(a, b) -> "(" ^ formula2latex a ^ ")" ^ (String.escaped "\rightarrow") ^ "(" ^ (formula2latex b) ^ ")"
  | Equivalent(a, b) -> "(" ^ formula2latex a ^ ")" ^ (String.escaped "\leftrightarrow") ^ "(" ^ (formula2latex b) ^ ")"
;;