all: tree combinations ensemble formule true valid main

combinations:
	ocamlopt -c combinations.ml

tree: 
	ocamlopt -c tree.ml

ensemble:
	ocamlopt -c ensemble.ml

formule:
	ocamlopt -c formule.ml

true:
	ocamlopt -c true.ml

valid:
	ocamlopt -c valid.ml

main: combinations tree ensemble formule true valid
	ocamlopt -o main combinations.cmx ensemble.cmx formule.cmx tree.cmx true.cmx valid.cmx main.ml 

test:

clean:
	rm -rf main
	rm -rf *.cmi *.cmx *.o
	rm -rf *.tmp
