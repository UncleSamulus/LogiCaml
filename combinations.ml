(** Combinatorics *)

let rec remove_all element list = match list with
  | [] -> []
  | e::tail when e=element -> remove_all element tail
  | e::tail -> e::(remove_all element tail) 
;;

let rec append_all possibilities list = 
  match possibilities with
  | [] -> []
  | possibility::others -> remove_all []
    (List.map (fun l -> possibility::l) list)@(append_all others list)
;;

let rec combinations possibilities n = 
  match n with
  | 0 -> [[]]
  | n -> 
    let rest = combinations possibilities (n - 1) in 
    append_all possibilities rest ;;