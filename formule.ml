(**Verify if a formule is well formed*)

include Ensemble ;;

type formule_t = Var of string
    | Neg of formule_t
    | Or of formule_t * formule_t
    | And of formule_t * formule_t
    | Implies of formule_t * formule_t
    | Equivalent of formule_t * formule_t ;;

let rec mem s l = match l with 
    | [] -> false
    | e::r -> (e = s) || mem s r 
;;


let rec get_signature formule =
    let rec get formule signature =
      match formule with 
      | Var s -> if mem s signature then signature else s::signature
      | Neg e -> get e signature
      | Or (e1, e2) -> union (get e1 signature) (get e2 []) 
      | And (e1, e2) -> union (get e1 signature) (get e2 [])
      | Implies (e1, e2) -> union (get e1 signature) (get e2 [])
      | Equivalent (e1, e2) -> union (get e1 signature) (get e2 [])
    in
    get formule []
  ;;
  
let rec verify formule signature = 
    match formule with
    | Var s -> mem s signature
    | Neg e -> verify e signature
    | Or (e1, e2) -> (verify e1 signature) && (verify e2 signature)
    | And (e1, e2) -> (verify e1 signature) && (verify e2 signature)
    | Implies (e1, e2) -> (verify e1 signature) && (verify e2 signature)
    | Equivalent (e1, e2) -> (verify e1 signature) && (verify e2 signature)
;;

